﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawner : MonoBehaviour {

	public GameObject[] objects;

	void Start () {

		InvokeRepeating ("SpawnRandom", 0, 1.0f);
	}

	// Update is called once per frame
	void Update() {


	}


	public void SpawnRandom()
	{
		Vector3 position = new Vector3 (Random.Range (-1.5f, 1.5f), 0.1f, 6f);
		Instantiate(objects[UnityEngine.Random.Range(0, objects.Length)], position, this.transform.rotation);
	}
}